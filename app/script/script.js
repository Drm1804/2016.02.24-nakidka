'use strict';

$(document).ready(function () {


    //Плавный скролл до элемента

    $('.b-sc1-menu-item a[href^="#"]').click(function () {
        var elementClick = $(this).attr("href");
        //console.log(destination)
        var destination = $(elementClick).offset().top;
        console.log(destination);
        jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 1100);
        return false;
    });

    //Слайдеры

    $('.b-production-slider').slick({
        infinite: true,
        prevArrow: '<span class="b-prod-sl-prev"></span>',
        nextArrow: '<span class="b-prod-sl-next"></span>',
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 900,
                settings:{
                    slidesToShow: 1
                }
            }
        ]
    });
    $('.b-review-slider').slick({
        infinite: true,
        dots: true,
        arrows: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 900,
                settings:{
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 650,
                settings:{
                    slidesToShow: 1
                }
            }
        ]
    });

    // Всплывающие окна
    $(".fancybox").fancybox({
        fitToView   : false,
        autoSize    : true
    });


    // Отправка форм

    $("form").ajaxForm(function() {
        $.fancybox([{href : '#after'}]);
    });


    //
    // custom slider - слайдер накидок
    //

    // Скрываем все картинки слайдера

    var allImgSlide = $('.custom-slider-img');

    allImgSlide.hide();

    // Показываем первый слайдер

    allImgSlide.each(function(index, value){
        if($(value).attr('data-id') == '1'){
            console.log($(value))
            $(value).show();
        }
    });


    // Ловим клик по кнопке
    $('.custom-slider-click').on('click', function(){
        var id = $(this).attr('data-id');
        $(this).parents('.custom-slider').find('.custom-slider-img').hide();
        $(this).parents('.custom-slider').find('.custom-slider-img[data-id="'+id+'"]').show();
    })
});



ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [54.985065523017134,57.29694003931472],
            zoom: 16,
            controls: []
        }, {
            searchControlProvider: 'yandex#search'
        }),
        HintLayout = ymaps.templateLayoutFactory.createClass("<div class='my-hint'>" +
            "<b>{{ properties.object }}</b><br />" +
            "{{ properties.address }}" +
            "</div>", {
            // Определяем метод getShape, который
            // будет возвращать размеры макета хинта.
            // Это необходимо для того, чтобы хинт автоматически
            // сдвигал позицию при выходе за пределы карты.
            getShape: function () {
                var el = this.getElement(),
                    result = null;
                if (el) {
                    var firstChild = el.firstChild;
                    result = new ymaps.shape.Rectangle(
                        new ymaps.geometry.pixel.Rectangle([
                            [0, 0],
                            [firstChild.offsetWidth, firstChild.offsetHeight]
                        ])
                    );
                }
                return result;
            }
        });
    var myPlacemark = new ymaps.Placemark([54.984726336864306,57.288649820098875], {
        address: "улица 40 лет Победы, 14 ",
        object: "Наш адрес:"
    }, {
        hintLayout: HintLayout
    });

    myMap.geoObjects.add(myPlacemark);
});
